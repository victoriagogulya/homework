use Books
go
Select * From Books;

/*1.  �������� �������� ���������, ������� ���������� �� ������������� 'BHV',
    � ����� ������� >= 3000 �����������.*/
use Books
go
Select Name From Books
Where Izd <> 'BHV' and Pressrun >= 3000;


/*2. �������� �����, �� ��� ������� ������� ������ �� ����� ����.
    T������ ���� ����� �������� � ������� ������� GETDATE(),
    � ������ ���������� ���� ����� � ������� ������� DATEFROMPARTS().*/
use Books
go
Select Name, [Date] From Books
Where [Date] Between 
DATEFROMPARTS(DATEPART(year, GETDATE()) - 1, DATEPART(month, GETDATE()), DATEPART(day, GETDATE()))
and GETDATE();


/*3. �������� �����, � ���� ������� ������� ������ �� ��������.*/
use Books
go
Select Name, Izd, [Date] From Books
Where [Date] is NULL;


/*4. �������� ��� �����-�������(c 2001�), ���� ������� ���� 30 ���.*/
use Books
go
Select Name, Price, [Date] From Books
Where [Date] Between 
DATEFROMPARTS(2001, 1, 1) and GETDATE() and Price < 30;


/*5. �������� �����, � ��������� ������� ���� ����� Microsoft, �� ��� ����� Windows.*/
use Books
go
Select Name From Books
Where Name Like '%Microsoft%' and Name not Like '%Windows%';


/* 6. �������� �����, � ������� ���� ����� �������� < 10 ������.*/
use Books
go
Select Name, Price, Pages From Books
Where Pages > 0 and (Price * 100 / Pages < 10);


/*7. �������� �����, � ��������� ������� ������������ ��� ������� ���� �����.*/
use Books
go
Select * From Books
Where Name Like '%[0-9]%';


/* 8. �������� �����, � ��������� ������� ������������ �� ����� ���� ����.*/
use Books
go
Select * From Books
Where Name Like '%[0-9]%[0-9]%[0-9]%';


/* 9. �������� �����, � ��������� ������� ������������ ����� ���� ����.*/
use Books
go
Select * From Books
Where Name Like '%[0-9]%[0-9]%[0-9]%[0-9]%[0-9]%' 
and Name not Like '%[0-9]%[0-9]%[0-9]%[0-9]%[0-9]%[0-9]%';


/*10. ������� �����, � ���� ������� ������������ ����� 6 ��� 7.*/
use Books
go
Delete From Books
Where Code Like '%6%' or Code like '%7%';
Select * From Books

/*11. ���������� ������� ���� ��� ��� ����, � ������� ���� ������� �����������.*/
use Books
go
Update Books
Set [Date] = GETDATE()
Where [Date] is NULL
Select * From Books