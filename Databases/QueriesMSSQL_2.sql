USE Library
GO

/*1. ������� ���������� � ����� � ���������� ����������� �������.*/
SELECT Authors.FirstName AS FirstName, Authors.LastName AS LastName, Books.Name, Books.Pages
FROM Books JOIN Authors ON Authors.Id = Books.Id_Author
WHERE Pages = (SELECT MAX(Pages) FROM Books);

/* 2. ������� ���������� � ����� �� ���������������� � ���������� ����������� �������.*/
SELECT Authors.FirstName AS FirstName, Authors.LastName AS LastName, Books.Name,
Books.Pages, Themes.Name AS Theme, YearPress, Comment, Quantity
FROM (Books JOIN Authors ON Authors.Id = Books.Id_Author)
JOIN Themes ON Themes.Id = Books.Id_Themes
WHERE Books.Pages = (SELECT MAX(Books.Pages) FROM Books
WHERE Books.Id_Themes = (SELECT Id FROM Themes WHERE Themes.Name = '����������������'));


/* 3. ������� ���������� ��������� ���������� �� ������ ������ ���������.*/
SELECT Groups.Name, COUNT(S_Cards.DateIn) AS CountOfVisits
FROM (Groups JOIN Students ON Groups.Id = Students.Id_Group)
JOIN S_Cards ON Students.Id = S_Cards.Id_Student
GROUP BY Groups.Name;

/*4. ������� ���������� ����, ������ � ���������� �������������� �� ���������
    ����������������� � ����� �������, � ����� ������� � ���� ������.*/
SELECT Themes.Name, COUNT(S_Cards.Id_Book) AS CountOfBooks, SUM(Books.Pages) AS TotalAmountOfPages
FROM ((((Faculties JOIN Groups ON Groups.Id_Faculty = Faculties.Id)
JOIN Students ON Students.Id_Group = Groups.Id)
JOIN S_Cards ON S_Cards.Id_Student = Students.Id)
JOIN Books ON Books.Id = S_Cards.Id_Book)
JOIN Themes ON Themes.Id = Books.Id_Themes
WHERE Faculties.Name = '����������������'
GROUP BY Themes.Name
HAVING Themes.Name IN ('����������������', '���� ������');


/*5. ��������, ��� ������� ����� ����� ������� ����� � ���� ���� ������ 1 �����,
    � �� ������ ������ ����� ���� �� ������ ����������� ���������� ������������ ������ ����������
    ���������� (������� ������� 0.5 �) �������� ����. ���������� ������� ������� ������
    ������ ������ �������, � ����� ��������� ���������� ������ ����. �������� ����� ������
    ����������� � ������� �������, �� ���� ��������� ����� ������ ������ ���� �����.
    ����������� ������� DATEDIFF � CAST.*/
SELECT Students.FirstName, Students.LastName, 
((DATEDIFF(day, S_Cards.DateOut, S_Cards.DateIn) / 7) * 0.5) AS AmountOfBeer
FROM (Students JOIN S_Cards ON Students.Id = S_Cards.Id_Student)
JOIN Libs ON Libs.Id = S_Cards.Id_Lib
WHERE Libs.FirstName = '������' AND Libs.LastName = '����������'


/* 6. ���� ������� ����� ���������� ���� � ���������� �� 100%, �� ���������� ����������
    ������� ���� (� ���������� ���������) ���� ������ ���������.*/
SELECT Faculties.Name, (COUNT(DISTINCT S_Cards.Id_Book) * 100 / (SELECT COUNT(Books.Id) FROM Books)) AS PercentOfBooks
FROM ((S_Cards JOIN Students ON Students.Id = S_Cards.Id_Student)
JOIN Groups ON Groups.Id = Students.Id_Group)
JOIN Faculties ON Faculties.Id = Groups.Id_Faculty
GROUP BY Faculties.Name


/* 7. ������� ������ ����������� ������(��) ����� ���������.*/
SELECT * FROM Authors
WHERE Authors.Id IN 
                   (SELECT IdA FROM
				         (SELECT Books.Id_Author AS IdA, COUNT(Books.Id_Author) AS CountOfBooks
                          FROM (Authors JOIN Books ON Authors.Id = Books.Id_Author)
                          JOIN S_Cards ON Books.Id = S_Cards.Id_Book
                          GROUP BY Books.Id_Author) AS A
						  WHERE A.CountOfBooks = 
						              (SELECT MAX(PagesOfBooks) FROM
									        (SELECT COUNT(Books.Id_Author) AS PagesOfBooks
										     FROM (Authors JOIN Books ON Authors.Id = Books.Id_Author)
                                             JOIN S_Cards ON Books.Id = S_Cards.Id_Book
                                             GROUP BY Books.Id_Author) AS B));


/*8. ������� ������ ����������� ������(��) ����� �������������� � ���������� ����
    ����� ������, ������ � ����������.*/
SELECT Authors.FirstName, Authors.LastName, CBC AS AmountOfBooks
FROM Authors, (SELECT IdA AS IdC, CountOfBooks AS CBC FROM
				            (SELECT Books.Id_Author AS IdA, COUNT(Books.Id_Author) AS CountOfBooks
                             FROM (Authors JOIN Books ON Authors.Id = Books.Id_Author)
                             JOIN T_Cards ON Books.Id = T_Cards.Id_Book
                             GROUP BY Books.Id_Author) AS A
				WHERE A.CountOfBooks = 
						     (SELECT MAX(PagesOfBooks) FROM
									        (SELECT COUNT(Books.Id_Author) AS PagesOfBooks
										     FROM (Authors JOIN Books ON Authors.Id = Books.Id_Author)
                                             JOIN T_Cards ON Books.Id = T_Cards.Id_Book
                                             GROUP BY Books.Id_Author) AS B)) AS C
WHERE Authors.Id = C.IdC;


/*9. ������� ������ ����������(��) ��������(�) ����� ��������� � ��������������.*/
SELECT 'Students' AS Visitors, Themes.Name
FROM Themes 
WHERE Themes.Name IN
                  (SELECT NameA FROM
                                (SELECT Themes.Name AS NameA, COUNT(Books.Id) AS CountA
				                 FROM (Themes JOIN Books ON Themes.Id = Books.Id_Themes)
				                 JOIN S_Cards ON Books.Id = S_Cards.Id_Book
								 GROUP BY Themes.Name) AS A
				  WHERE A.CountA = 
				                 (SELECT MAX(CountB) FROM
									        (SELECT COUNT(Books.Id) AS CountB
										     FROM (Themes JOIN Books ON Themes.Id = Books.Id_Themes)
				                             JOIN S_Cards ON Books.Id = S_Cards.Id_Book
                                             GROUP BY Themes.Name) AS B))
UNION ALL
SELECT 'Teachers' AS Visitors, Themes.Name
FROM Themes 
WHERE Themes.Name IN
                  (SELECT NameA FROM
                                (SELECT Themes.Name AS NameA, COUNT(Books.Id) AS CountA
				                 FROM (Themes JOIN Books ON Themes.Id = Books.Id_Themes)
				                 JOIN T_Cards ON Books.Id = T_Cards.Id_Book
								 GROUP BY Themes.Name) AS A
				  WHERE A.CountA = 
				                 (SELECT MAX(CountB) FROM
									        (SELECT COUNT(Books.Id) AS CountB
										     FROM (Themes JOIN Books ON Themes.Id = Books.Id_Themes)
				                             JOIN T_Cards ON Books.Id = T_Cards.Id_Book
                                             GROUP BY Themes.Name) AS B));


/*10. ����������� ����� ����� ��� ���������, ������� �� ������� ����� ����� ����,
    �.�. � ������� ��������� ��������� (S_Cards) ��������� ���� "���� ��������"
    (DateIn) ������� �����.*/
UPDATE S_Cards 
SET S_Cards.DateIn = GETDATE()
WHERE (DATEDIFF(year, S_Cards.DateOut, S_Cards.DateIn) > 1);

/*11. ������� �� ������� ��������� ��������� ���������, ������� ��� ������� �����.*/
DELETE FROM S_Cards
WHERE DateIn IS NOT NULL;
