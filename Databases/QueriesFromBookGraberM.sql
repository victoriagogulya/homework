#insert into sellers(name, city, comm) 
#values('Motika', 'London', 11), 
#	('Rifkin', 'Barcelona', 15),
#    ('Axelrod', 'NewYork', 10);

#insert into customers(name, city, rating, SellerFk)
#values('Liu', 'SanJose', 200, 2),
#	('Grass', 'Berlin', 300, 2),
#    ('Clemens', 'London', 100, 1),
#    ('Cisneros', 'SanJose', 300, 3),
#    ('Pereira', 'Rome', 100, 4);

#insert into orders(amt, odate, customerFk, SellerFk)
#values
#	(18.69, '1990-03-10', 6, 4),
#	(767.19, '1990-03-10', 1, 1),	
#   (1900.10, '1990-03-10', 6, 3),
#   (5160.45, '1990-03-10', 3, 2),
#    (1098.16, '1990-03-10', 6, 4),
#    (1713.23, '1990-04-10', 3, 2),
#    (75.75, '1990-04-10', 4, 2),
#    (4723.00, '1990-05-10', 5, 1),
#    (1309.95, '1990-06-10', 4, 2),
#   (9891.88, '1990-06-10', 5, 1);
 
#выбрать имена и номера всех заказчиков с максимальными для их
#городов оценками
#select * from customers
#group by customers.city
#having max(rating);

#выбрать всех продавцов ( по их имени и номеру ), которые в своих городах
#имеют заказчиков, которых они не обслуживают.
/*select * from sellers s
where 0 < 
		(select count(*) from customers
        where customers.city != s.city);
*/
        
#Напишите запрос который бы использовал оператор EXISTS для извлечения 
#всех продавцов которые имеют заказчиков с оценкой 300        
/*select * from sellers s
where exists
	(select * from orders o
    where s.id = o.sellerFk
    and exists
		(select * from customers
        where customers.id = o.customerFk
        and customers.rating = 300));
 */
 
 #Как бы вы решили предыдущую проблему используя обьединение
 /*select distinct sellers.id, sellers.name
 from sellers, customers, orders
 where sellers.id = orders.sellerFk
 and customers.id = orders.customerFk
 and customers.rating = 300;
 */
 
#Напишите запрос использующий оператор EXISTS который выберет всех
#продавцов с заказчиками размещенными в их городах, которые ими не
#обслуживаются
/*select s.name, c.name, c.city
from sellers s, customers c
where s.city = c.city
and not exists
	(select * from orders
    where orders.customerFk = c.id
    and orders.sellerFk = s.id);
 */
 
#Напишите запрос который бы выбирал всех заказчиков чьи оценки равны
#или больше чем любая( ANY ) оценка заказчика Liu.
 /*select * from customers
 where rating >= ANY
	(select rating from customers
    where name = 'Liu');
*/

#Создайте объединение из двух запросов которое показало бы имена,
#города, и оценки всех заказчиков. Те из них которые имеют поле rating=200 и более, 
#должны кроме того иметь слова - "Высокий Рейтинг", а остальные должны иметь слова 
#" Низкий Рейтинг ".
/*select c1.name, c1.city, c1.rating, 'Высокий Рейтинг' as 'state'
from customers c1
where c1.rating >= 200
union
select c2.name, c2.city, c2.rating, ' Низкий Рейтинг '
from customers c2
where c2.rating < 200
order by 3 desc;
*/

#Напишите команду которая бы вывела имена каждого продавцa и
#каждого заказчика которые имеют больше чем один заказ.
#Результат представьте в алфавитном порядке.
/*select s.name, 'seller' as 'status'
from sellers s
where EXISTS
	(select * from orders
    where sellerFk = s.id)
union 
select c.name, 'customer'
from customers c
where EXISTS
	(select * from orders
    where customerFk = c.id)
order by 1; 
*/

#Сформируйте объединение из трех запросов. Первый выбирает поля id всех 
#продавцов в San Jose; второй, поля id всех заказчиков в San Jose; 
#и третий поля id всех порядков на 3 Октября. Сохраните дубликаты между последними 
#двумя запросами, но устраните любую избыточность вывода между каждым из них и 
#самым первым.
/*select id from sellers
where city = 'SanJose'
union
select id from customers
where city = 'SanJose'
union all 
select id from orders
where odate = '1990-03-10';
*/

                ### INSERT, UPDATE, DELETE ###
#Напишите команду которая бы поместила следующие значения, в их 
#нижеуказанном порядке, в таблицу Продавцов:
#city - San Jose, name - Bianco, comm - NULL.
/*insert into sellers(city, name, comm) 
values('SanJose', 'Bianco', NULL);
*/

#Напишите команду, которая бы увеличила оценку всех заказчиков в Риме на 100.
#update customers set rating = rating + 100
#where city = 'Rome';

#update customers set rating = rating - 100
#where city = 'Rome';

/*delete from sellers
where name = 'Bianco';
*/







        
        
        