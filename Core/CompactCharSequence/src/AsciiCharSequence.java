import java.util.Arrays;

public class AsciiCharSequence implements CharSequence {

    private final byte[] data;
    private int size;

    public AsciiCharSequence(byte[] content) {
        data = content;
        size = data.length;
    }

    @Override
    public int length() {
        return size;
    }

    @Override
    public char charAt(int index) {
        if (index < 0 || index >= size) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return (char)data[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new StringIndexOutOfBoundsException(start);
        }
        if (end > size) {
            throw new StringIndexOutOfBoundsException(end);
        }
        if (start > end) {
            throw new StringIndexOutOfBoundsException(end - start);
        }
        return  new AsciiCharSequence(Arrays.copyOfRange(data, start, end));
    }

    @Override
    public String toString() {
        return new String(data);
    }
}
