

public class Main {

    public static void main(String[] args) {
        TextAnalyzer[] a = new TextAnalyzer[]{new NegativeTextAnalyzer(),
        new SpamAnalyzer(new String[]{"first", "second"}), new TooLongTextAnalyzer(30)};
        String str = "this is first test, and it is spam";
        System.out.println(checkLabels(a, str));
    }

    public static Label checkLabels(TextAnalyzer[] analyzers, String text) {

        Label label = Label.OK;

        for(TextAnalyzer an : analyzers) {
           label = an.processText(text);
            if(label != Label.OK)
                break;
        }
        return label;
    }
}
