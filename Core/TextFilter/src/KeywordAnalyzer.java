
public abstract class KeywordAnalyzer implements TextAnalyzer {

    @Override
    public Label processText(String text) {
        Label label = Label.OK;

        for(String str : getKeywords()) {
            if(text.contains(str)) {
                label = getLabel();
                break;
            }
        }
        return label;
    }

    protected abstract String[] getKeywords();

    protected abstract Label getLabel();
}
