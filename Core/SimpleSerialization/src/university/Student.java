package university;


import java.io.Serializable;

public class Student implements Serializable {

    private String name;
    private String surname;
    private int age;

    public Student() {
        name = "Unknown";
        surname = "Unknown";
    }

    public Student(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    @Override
    public String toString() {
        return surname + ' ' + name + ' ' + age;
    }
}
