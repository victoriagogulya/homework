package university;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Group implements Serializable {

    private List<Student> students = new ArrayList<>();
    private Faculty faculty;

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public List<Student> getStudents() {
        return students;
    }

    public int getCount() {
        return students.size();
    }

    public void add(Student student) {
        students.add(student);
    }

    public void add(Collection students) {
        this.students.addAll(students);
    }
}
