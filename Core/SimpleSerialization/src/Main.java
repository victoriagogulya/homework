import university.Faculty;
import university.Group;
import university.Student;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Group group = new Group();
        group.setFaculty(Faculty.ComputerScience);
        List<Student> students = new ArrayList<>();
        students.add(new Student("Ivanov", "Ivan", 20));
        students.add(new Student("Petrov", "Petr", 20));
        group.add(students);

        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("group.dat"));
            out.writeObject(group);
        } catch (IOException e) {
            System.out.println("Exception in out");
        }
        System.out.println("Done!");

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("group.dat"));
            try {
                group = (Group)in.readObject();
                students = group.getStudents();
                students.forEach(System.out::print);
            } catch (ClassNotFoundException e) {
                System.out.println("Class not found exception");
            }
        } catch (IOException e) {
            System.out.println("Exception in in");
        }
    }
}
